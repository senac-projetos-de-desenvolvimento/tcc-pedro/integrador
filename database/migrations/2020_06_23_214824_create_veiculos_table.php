<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('users_id')->nullable($value = true);
            $table->unsignedInteger('chassis_id')->nullable($value = true);
            $table->unsignedInteger('carrocerias_id')->nullable($value = true);
            $table->unsignedInteger('empresas_id')->nullable($value = true);
            $table->integer('prefixo')->unique();
            $table->integer('banco')->nullable($value = true);
            $table->integer('lotacao')->nullable($value = true);
            $table->string('placa',100)->nullable($value = true);
            $table->date('licenca_data')->nullable($value = true);
            $table->integer('ano_modelo')->nullable($value = true);
            $table->integer('ano_fabricacao')->nullable($value = true);
            $table->date('vistoria_data')->nullable($value = true);
            $table->tinyInteger('acessibilidade')->nullable($value = true);
            $table->string('descricao',250)->nullable($value = true);
            $table->tinyInteger('ativo');
            $table->timestamps();
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('empresas_id')->references('id')->on('empresas');
            $table->foreign('carrocerias_id')->references('id')->on('carrocerias');
            $table->foreign('chassis_id')->references('id')->on('chassis');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculos');
    }
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('empresas_id')->nullable($value = true);
            $table->unsignedInteger('users_id');
            $table->string('nome',100)->unique();
            $table->string('cpf',20)->unique()->nullable($value = true);
            $table->unsignedInteger('matricula')->nullable($value = true);
            $table->tinyInteger('profissao')->nullable($value = true);
            $table->date('nascimento')->nullable($value = true);
            $table->date('curso_data')->nullable($value = true);
            $table->string('endereco',100)->nullable($value = true);
            $table->string('telefone',100)->nullable($value = true);
            $table->string('email',100)->nullable($value = true);
            $table->string('descricao',250)->nullable($value = true);
            $table->tinyInteger('ativo');
            $table->timestamps();
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('empresas_id')->references('id')->on('empresas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}
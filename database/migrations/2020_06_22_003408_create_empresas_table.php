<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('users_id');
            $table->string('nome',100)->unique();
            $table->string('cnpj',30)->unique()->nullable($value = true);
            $table->string('endereco',100)->nullable($value = true);
            $table->string('cep',10)->nullable($value = true);
            $table->string('telefone',20)->nullable($value = true);
            $table->string('email',100)->nullable($value = true);
            $table->string('descricao',250)->nullable($value = true);
            $table->tinyInteger('ativo');
            $table->timestamps();
            $table->foreign('users_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}

<?php

use Illuminate\Database\Seeder;

class ViagemSabadosTresVendasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //tabela 401M
      DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1040',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '05:30',         
        'fimprogramado' => '06:00',         
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1040',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '06:00',         
        'fimprogramado' => '06:25',         
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
   
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1040',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '07:30',         
        'fimprogramado' => '08:00',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1040',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '08:00',         
        'fimprogramado' => '08:30',
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
   
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1040',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '10:00',         
        'fimprogramado' => '10:30',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1040',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '10:30', 
        'fimprogramado' => '10:55',        
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1042',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '11:00',         
        'fimprogramado' => '11:30',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1042',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '11:30',         
        'fimprogramado' => '11:55',
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1042',
        'motorista' => '1007',
        'cobrador' => '2007',
        'tabela' => '401M',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '13:00',         
        'fimprogramado' => '13:30',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '401',
        'codatendimento' => '1042',
        'motorista' => '1008',
        'cobrador' => '2008',
         'tabela' => '401T',
        'tipodia' => 'SABADOS',
        'inicioprogramado' => '13:30',         
        'fimprogramado' => '13:55',
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
  //tabela 401T
  DB::table('viagens')->insert([
    'veiculo' => '401',
    'codatendimento' => '1041',
    'motorista' => '1008',
    'cobrador' => '2008',
    'tabela' => '401T',
    'tipodia' => 'SABADOS',
    'inicioprogramado' => '14:00',         
    'fimprogramado' => '14:30',
    'data' => '02/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '401',
        'codatendimento' => '1041',
        'motorista' => '1008',
        'cobrador' => '2008',
        'tabela' => '401T',
        'tipodia' => 'SABADOS',
    'inicioprogramado' => '14:30',         
    'fimprogramado' => '14:55',
    'data' => '02/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);

DB::table('viagens')->insert([
    'veiculo' => '401',
        'codatendimento' => '1042',
        'motorista' => '1008',
        'cobrador' => '2008',
        'tabela' => '401T',
        'tipodia' => 'SABADOS',
    'inicioprogramado' => '16:30',         
    'fimprogramado' => '17:00',
    'data' => '02/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '401',
    'codatendimento' => '1042',
    'motorista' => '1008',
    'cobrador' => '2008',
    'tabela' => '401T',
    'tipodia' => 'SABADOS',
    'inicioprogramado' => '17:00',         
    'fimprogramado' => '17:55',
    'data' => '02/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '401',
    'codatendimento' => '1041',
    'motorista' => '1008',
    'cobrador' => '2008',
    'tabela' => '401T',
    'tipodia' => 'SABADOS',
    'inicioprogramado' => '18:00',         
    'fimprogramado' => '18:30',
    'data' => '02/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '401',
    'codatendimento' => '1041',
    'motorista' => '1008',
    'cobrador' => '2008',
    'tabela' => '401T',
    'tipodia' => 'SABADOS',
    'inicioprogramado' => '18:30',         
    'fimprogramado' => '18:55',
    'data' => '02/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);

DB::table('viagens')->insert([
    'veiculo' => '401',
    'codatendimento' => '1042',
    'motorista' => '1008',
    'cobrador' => '2008',
    'tabela' => '401T',
    'tipodia' => 'SABADOS',
    'inicioprogramado' => '20:00',         
    'fimprogramado' => '20:30',
    'data' => '02/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '401',
        'codatendimento' => '1042',
        'motorista' => '1008',
        'cobrador' => '2008',
        'tabela' => '401T',
        'tipodia' => 'SABADOS',
    'inicioprogramado' => '20:30',         
    'fimprogramado' => '20:55',
    'data' => '02/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '401',
    'codatendimento' => '1040',
    'motorista' => '1008',
    'cobrador' => '2008',
    'tabela' => '401T',
    'tipodia' => 'SABADOS',
    'inicioprogramado' => '21:00',         
    'fimprogramado' => '21:20',
    'data' => '02/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '401',
    'codatendimento' => '1040',
    'motorista' => '1008',
    'cobrador' => '2008',
    'tabela' => '401T',
    'tipodia' => 'SABADOS',
    'inicioprogramado' => '21:20',         
    'fimprogramado' => '21:55',
    'data' => '02/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);

    }
}

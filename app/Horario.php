<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Atendimento;
use App\Linha;
use App\Tipo;
class Horario extends Model
{
    // identifica os campos que serão inseridos, ou seja, que estamos
    // dando permissão de serem atualizados na store
    protected $fillable = ['horario','codatendimento', 'sentido','data','tipodia','users_id'];
    public function tipos() {
        return $this->belongsTo('App\Tipo');
    }
    public function linhas() {
        return $this->belongsTo('App\Linha');
    }
    public function users() {
        return $this->belongsTo('App\User');
    }
    public function atendimentos() {
        return $this->belongsTo('App\Atendimento');
    }
    
    public static function sentido() {
        return ['Bairro', 'Centro'];
    }
    
}

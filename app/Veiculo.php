<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Empresa;
class Veiculo extends Model
{
    //
    protected $table = 'veiculos';
    protected $primaryKey = 'id';
    protected $fillable = ['id','users_id','empresas_id','prefixo','banco','lotacao','placa',
    'licenca_data','ano_modelo','ano_fabricacao','vistoria_data','chassis_id','carrocerias_id',
    'acessibilidade','descricao','ativo'];
    public function empresas() {
        return $this->belongsTo('App\Empresa');
    }
    public function carrocerias() {
        return $this->belongsTo('App\Carroceria');
    }

    public function chassis() {
        return $this->belongsTo('App\Chassi');
    }
    
    public function users() {
        return $this->belongsTo('App\User');
    }
    public static function ativo() {
        return ['Ativo', 'Inativo'];
    }
    public function getAtivoAttribute($value) {
        if ($value=="1") {
            return "Ativo";
        } else if ($value == "0") {
            return "Inativo";
        } 
    }
    public function setAtivoAttribute($value) {
        if ($value == "Ativo") {
            $this->attributes['ativo'] = "1";
        } else if ($value == "Inativo") {
            $this->attributes['ativo'] = "0";
        } 
    }        
    public static function acessibilidade() {
        return ['Sim', 'Não'];
    }
    public function getAcessibilidadeAttribute($value) {
        if ($value=="1") {
            return "Sim";
        } else if ($value == "0") {
            return "Não";
        } 
    }
    public function setAcessibilidadeAttribute($value) {
        if ($value == "Sim") {
            $this->attributes['acessibilidade'] = "1";
        } else if ($value == "Não") {
            $this->attributes['acessibilidade'] = "0";
        } 
    }        
    
}
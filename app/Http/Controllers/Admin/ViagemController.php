<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Viagem;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ViagemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dados = Viagem::paginate(30);

        $numViagens = Viagem::count('id');    // obtém o número de registros

        return view('admin.viagens_list', ['linhas'=>$dados,'numViagens'=>$numViagens]);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
        return view('admin.viagens_form', ['acao'=>1]);
        $users = User::orderBy('name')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
        $dados = $request->all();

        $inc = Viagem::create($dados);

        if ($inc) {
            return redirect()->route('viagens.index')
                 ->with('status', $request->nome . ' inserido com sucesso');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Viagem  $viagem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Viagem  $viagem
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $reg = Viagem::find($id);
       
       

        return view('admin.viagens_form', ['reg' => $reg, 'acao' => 2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Viagem  $viagem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          // obtém os dados do form
          $dados = $request->all();

          // posiciona no registo a ser alterado
          $reg = Viagem::find($id);
  
         
          // realiza a alteração
          $alt = $reg->update($dados);
  
          if ($alt) {
              return redirect()->route('viagens.index')
                              ->with('status', $request->numeroviagem . ' Alterado!');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Viagem  $viagem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $viagens = Viagem::find($id);
        if ($viagens->delete()) {
            return redirect()->route('viagens.index')
                            ->with('status', $viagens->nome . ' Excluído!');
        }
    }
}
<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Empresa;
use App\Funcionario;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dados = Empresa::paginate(30);

        $numEmpresas = Empresa::count('id');    // obtém o número de registros

        return view('admin.empresas_list', ['empresas'=>$dados,'numEmpresas'=>$numEmpresas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::orderBy('name')->get();
        $empresas = Empresa::orderBy('nome')->get();
        return view('admin.empresas_form', ['users' => $users,'empresas' => $empresas,'acao' => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nome' => 'min:4|max:100|unique:empresas',
            'cnpj'=> 'max:14',
            'email'=> 'max:100',
            'telefone'=> 'max:11',
            'cep'=> 'max:8',
            'ativo'=> 'required' 
      
           
       ]);  
        //
        $dados = $request->all();

        $inc = Empresa::create($dados);

        if ($inc) {
            return redirect()->route('empresas.index')
                 ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $reg = Empresa::find($id);
       
        return view('admin.empresas_form', ['reg' => $reg,  
        'acao' => 2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nome' => 'min:4|max:100',
            'cnpj'=> 'max:14',
            'email'=> 'max:100',
            'telefone'=> 'max:11',
            'cep'=> 'max:8',
            'ativo'=> 'required' 
      
           
       ]);
       //
          // obtém os dados do form
          $dados = $request->all();

          // posiciona no registo a ser alterado
          $reg = Empresa::find($id);
  
         
          // realiza a alteração
          $alt = $reg->update($dados);
  
          if ($alt) {
              return redirect()->route('empresas.index')
                              ->with('status', $request->nome . ' Alterado!');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $empresa = Empresa::find($id);
        if ($empresa->delete()) {
            return redirect()->route('empresas.index')
                            ->with('status', $empresa->nome . ' Excluído!');
        }
    }

    public function empresasFiltro()
    {
        $empresas = Empresa::orderBy('nome')->get();
        return view('admin.empresas_filtro', [ 'acao' => 1,
           ]);

    }

    public function empresasRelatorio(Request $request)
    {

        $this->validate($request, [
            'inicio' => 'required',
            'fim' => 'required'

       ]); 
        $inicio = $request->inicio;
        $fim = $request->fim;
        

$total="select e.nome, sum(if(v.nomepontoinicio LIKE 'CENTRO', a.km_ida,a.km_volta))as KM 
FROM empresas e, viagens v, atendimentos a, veiculos c WHERE c.prefixo = v.veiculo 
AND v.codatendimento = a.codatendimento and e.id = c.empresas_id and v.data >= '$inicio'
AND v.data <= '$fim' GROUP by 1";

$dia="select e.nome, v.data, sum(if(v.nomepontoinicio LIKE 'CENTRO', a.km_ida,a.km_volta))as KM 
FROM empresas e, viagens v, atendimentos a, veiculos c WHERE c.prefixo = v.veiculo 
AND v.codatendimento = a.codatendimento and e.id = c.empresas_id and v.data >= '$inicio'
AND v.data <= '$fim' GROUP by 1, 2";

$soma="select sum(if(v.nomepontoinicio LIKE 'CENTRO', a.km_ida,a.km_volta))as KM 
FROM empresas e, viagens v, atendimentos a, veiculos c WHERE c.prefixo = v.veiculo 
AND v.codatendimento = a.codatendimento and e.id = c.empresas_id and v.data >= '$inicio'
AND v.data <= '$fim' ";

$kmempresas= DB::select($total);   
$kmdias= DB::select($dia);   
$kmsoma= DB::select($soma);   
 return \PDF::loadView('admin.empresas_rel', ['kmempresas' => $kmempresas, 'kmdias' => $kmdias,
 'kmsoma' => $kmsoma,'inicio'=>$inicio, 'fim'=>$fim])->stream();

    }

    
}
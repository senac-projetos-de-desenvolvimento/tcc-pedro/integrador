<?php
namespace App\Http\Controllers\Admin;

use App\Atendimento;
use App\Horario;
use App\Http\Controllers\Controller;
use App\Linha;
use App\Tipo;
use App\User;
use App\Viagem;
use App\Funcionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
$slug = Str::slug('Laravel 5 Framework', '-');
class HorarioController extends Controller
{
    public function index()
    {
        $informacao = 'Todos Horários';
        $dados = Horario::orderBy('horario')->paginate(30);
        $numHorarios = Horario::count('id');
        return view('admin.horarios_list', ['linhas' => $dados, 'informacao' => $informacao,
            'numHorarios' => $numHorarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $atendimentos = Atendimento::orderBy('nome')->get();
       
        $tipos = Tipo::orderBy('nome')->get();
        $users = User::orderBy('name')->get();
        
        return view('admin.horarios_form', ['tipos' => $tipos, 
            'atendimentos' => $atendimentos, 'users' => $users, 'acao' => 1,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // obtém todos os campos vindos do form
        $dados = $request->all();
        $inc = Horario::create($dados);
        if ($inc) {
            return redirect()->route('horarios.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Horario::find($id);
        $users = User::orderBy('name')->get();
        $linhas = Linha::orderBy('nome')->get();
        $tipos = Tipo::orderBy('nome')->get();
        $sentidos = Horario::sentido();
        $atendimentos = Atendimento::orderBy('nome')->get();
        return view('admin.horarios_form', ['reg' => $reg, 'tipos' => $tipos, 'linhas' => $linhas,
            'users' => $users, 'sent' => $sentidos, 'atendimentos' => $atendimentos,
            'acao' => 2]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'horario' => 'required',
            'sentido' => 'required',
            
        ]);

        // obtém os dados do form
        $dados = $request->all();
        // posiciona no registo a ser alterado
        $reg = Horario::find($id);
        // realiza a alteração
        $alt = $reg->update($dados);
        if ($alt) {
            return redirect()->route('horarios.index')
                ->with('status', $request->nome . ' Alterado!');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horario = Horario::find($id);
        if ($horario->delete()) {
            return redirect()->route('horarios.index')
                ->with('status', $horario->nome . ' Excluído!');
        }
    }

    public function guia()
    {

        return view('admin.guia');
    }
    public function horariosFiltro()
    {

        $atendimentos = Atendimento::orderBy('nome')->get();
        $linhas = Linha::orderBy('nome')->get();
        $tipos = Tipo::orderBy('nome')->get();
        $users = User::orderBy('name')->get();
        $sentidos = Horario::sentido();
       

        return view('admin.horarios_filtro', ['tipos' => $tipos, 'linhas' => $linhas,
            'atendimentos' => $atendimentos, 'users' => $users, 'acao' => 1,
            'sent' => $sentidos]);

    }
    public function horariosImportFiltro()
    {
     
        $sql = "SELECT DISTINCT data from viagens order by 'desc'";
        $viagens = DB::select($sql);

        return view('admin.horariosimport_filtro', ['viagens' => $viagens,'acao' => 1]);

    }
    public function horariosImport(Request $request)
    {
        $uteis = $request->uteis;
        $sabados = $request->sabados;
        $domingos = $request->domingos;

        DB::table('horarios')->truncate();
        $sql = "SELECT codatendimento, inicioprogramado as horario, nomepontoinicio as sentido, 
         data, tipodia  from viagens
       where viagens.tabela and viagens.data Like '$uteis'
       or viagens.data Like '$sabados'
       or viagens.data Like '$domingos'";
        $viagens = DB::select($sql);
        $data= json_decode( json_encode($viagens), true);     
                
                  DB::table('horarios')->insertOrIgnore($data);
                    return view('admin.importacao')
                ->with('status', 'Parabéns! Horários Atualizados com Sucesso.');
                
    }
    public function guiasFiltro()
    {

     //  $viagens = Viagem::orderBy('tabela')->distinct()->get();
     
        
        $sql = "SELECT DISTINCT tabela from viagens";
        $viagens = DB::select($sql);
        return view('admin.guia_filtro', ['viagens' => $viagens, 'acao' => 1]);

    }

    public function importacao()
    {
        
        $url = ("http://sensationnel-vin-78942.herokuapp.com/api/viagens");
        $ch = curl_init('http://sensationnel-vin-78942.herokuapp.com/api/viagens');  
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
        $data = curl_exec($ch);  
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
    curl_close($ch);  

if( $httpcode >= 200 && $httpcode < 300 ){  
    $ch = curl_init($url);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
           $viagens = json_decode(curl_exec($ch));
           if ($objs = json_decode(curl_exec($ch), true)) {
               foreach ($objs as $obj) {
                   foreach ($obj as $key => $value) {
                       $insertArr [str_slug($key, '_')] = $value;
                   }
                   if ($obj != null) {
                       DB::table('viagens')->insertOrIgnore($insertArr);
                       return view('admin.importacao')
                ->with('status', 'Parabéns! Viagens Atualizadas com Sucesso.');
                   } else {
                       return view('admin.importacao')->with('status', 'Ops! Algo de errado não está certo.');
                   }
               }
           } 
} else {
    return view('admin.importacao')->with('status', 'Ops! Verifique se o API está Online!!');
}
    }
    
    
    public function guiaPDF(Request $request)
    {
        $data = $request->data;
        $tabela = $request->tabela;
        $this->validate($request, [
            'data' => 'required'
       ]); 
        $motorista = "SELECT DISTINCT v.motorista, f.nome as motorista, veiculo 
        from viagens v, funcionarios f
        where v.motorista = f.matricula 
        and v.tabela LIKE '$tabela'
     and v.data Like '$data'";
        
        $cobrador = "SELECT DISTINCT v.cobrador, f.nome as cobrador, veiculo 
        from viagens v, funcionarios f
        where v.cobrador = f.matricula 
        and v.tabela LIKE '$tabela'
     and v.data Like '$data'";

        $sql = "SELECT viagens.inicioprogramado, viagens.fimprogramado,
        viagens.nomepontoinicio, atendimentos.nome from viagens, atendimentos
        where atendimentos.codatendimento = viagens.codatendimento
        and viagens.tabela LIKE '$tabela'
     and viagens.data Like '$data'";
        $viagens = DB::select($sql);
        $distmotorista = DB::select($motorista);
        $distcobrador = DB::select($cobrador);
       
         return \PDF::loadView('admin.guia_rel', ['viagens' => $viagens,
        'data'=>$data, 'tabela'=> $tabela, 'distmotorista'=> $distmotorista,
        'distcobrador'=>$distcobrador ])->stream();

    }

    public function horariosRelatorio(Request $request)
    {
        $linhas_id = $request->linhas_id;

        $sqluc = "SELECT horario, atendimentos.nome AS atendimento,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
        INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
        INNER join linhas on atendimentos.linhas_id = linhas.id
         where tipos.nome = 'UTEIS' and horarios.sentido = 'CENTRO'
         and linhas.id = $linhas_id
         order by horarios.horario";

        $centroU = DB::select($sqluc);

        $sqlbu = "SELECT horario, atendimentos.nome AS atendimento,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
        INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
        INNER join linhas on atendimentos.linhas_id = linhas.id
         where tipos.nome = 'UTEIS' and horarios.sentido = 'BAIRRO'
         and linhas.id = $linhas_id
         order by horarios.horario";

        $bairroU = DB::select($sqlbu);

        $sqlcs =  "SELECT horario, atendimentos.nome AS atendimento,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
        INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
        INNER join linhas on atendimentos.linhas_id = linhas.id
         where tipos.nome = 'SABADOS' and horarios.sentido = 'CENTRO'
         and linhas.id = $linhas_id
         order by horarios.horario";
        $centroS = DB::select($sqlcs);

        $sqlbs =  "SELECT horario, atendimentos.nome AS atendimento,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
        INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
        INNER join linhas on atendimentos.linhas_id = linhas.id
         where tipos.nome = 'SABADOS' and horarios.sentido = 'BAIRRO'
         and linhas.id = $linhas_id
         order by horarios.horario";

        $bairroS = DB::select($sqlbs);

        $sqlcd = "SELECT horario, atendimentos.nome AS atendimento,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
        INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
        INNER join linhas on atendimentos.linhas_id = linhas.id
         where tipos.nome = 'DOMINGOS' and horarios.sentido = 'CENTRO'
         and linhas.id = $linhas_id
         order by horarios.horario";

        $centroD = DB::select($sqlcd);

        $sqlbd = "SELECT horario, atendimentos.nome AS atendimento,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
        INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
        INNER join linhas on atendimentos.linhas_id = linhas.id
         where tipos.nome = 'DOMINGOS' and horarios.sentido = 'BAIRRO'
         and linhas.id = $linhas_id
         order by horarios.horario";

        $bairroD = DB::select($sqlbd);
        
        for ($i=0; $i < 100; $i++) 
        { 
            $items[$i] = $i + 1;
        }
       
                  
        
    
        return \PDF::loadView('admin.horarios_rel', ['centroU' => $centroU, 'bairroU' => $bairroU,
            'centroS' => $centroS, 'bairroS' => $bairroS,
            'centroD' => $centroD, 'bairroD' => $bairroD,'items' => $items])->stream();

    }
    public function graficoUteis()
    {
        $tipos = " de Tabelas de Dias Úteis";

        $sql = "select  linhas.nome, count(horarios.id) as total from horarios,atendimentos,
        linhas where linhas.id = atendimentos.linhas_id 
        and horarios.tipodia = 'UTEIS' 
        and horarios.codatendimento = atendimentos.codatendimento
        group by linhas.nome order by 2 desc, 1";
        $totais = DB::select($sql);

        if ($totais == null) {
            return redirect()->route('horarios.index')
                ->with('status', 'Não existe Tabela de Dias Úteis');

        } else {
            return view('admin.horarios_graf', ['totais' => $totais, 'tipos' => $tipos]);
        }
    }

    public function graficoSabados()
    {
        $tipos = " de Tabelas Sábados";
        $sql = "select  linhas.nome, count(horarios.id) as total from horarios,atendimentos,
        linhas where linhas.id = atendimentos.linhas_id 
        and horarios.tipodia = 'SABADOS' 
        and horarios.codatendimento = atendimentos.codatendimento
        group by linhas.nome order by 2 desc, 1";
        $totais = DB::select($sql);

        if ($totais == null) {
            return redirect()->route('horarios.index')
                ->with('status', 'Não existe Tabela de Sábados');

        } else {
            return view('admin.horarios_graf', ['totais' => $totais, 'tipos' => $tipos]);
        }
    }
    public function graficoDomingos()
    {
        $tipos = " de Tabelas de Domingos";
        $sql = "select  linhas.nome, count(horarios.id) as total from horarios,atendimentos,
        linhas where linhas.id = atendimentos.linhas_id 
        and horarios.tipodia = 'DOMINGOS' 
        and horarios.codatendimento = atendimentos.codatendimento
        group by linhas.nome order by 2 desc, 1";
        $totais = DB::select($sql);

        if ($totais == null) {
            return redirect()->route('horarios.index')
                ->with('status', 'Não existe Tabela de Domingos');

        } else {
            return view('admin.horarios_graf', ['totais' => $totais, 'tipos' => $tipos]);
        }
    }
}

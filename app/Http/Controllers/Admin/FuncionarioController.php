<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Funcionario;
use App\User;
use App\Empresa;
use App\Viagem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FuncionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dados = Funcionario::paginate(30);
        // obtém a soma do campo preço
        $numFuncionarios = Funcionario::count('id'); // obtém o número de registros
        return view('admin.funcionarios_list', ['linhas' => $dados,
            'numFuncionarios' => $numFuncionarios]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('name')->get();
        $empresas = Empresa::orderBy('nome')->get();
        $funcionarios = Funcionario::orderBy('nome')->get();
        return view('admin.funcionarios_form', ['empresas' => $empresas,
            'users' => $users, 'acao' => 1]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'min:4|max:100|unique:funcionarios',
            'empresas_id'=> 'required',
            'matricula'=> 'max:6',
            'profissao'=> 'required',
            'cpf'=> 'max:11|unique:funcionarios',
            'endereco'=> 'max:100',
            'telefone'=> 'max:11',
            'email'=> 'max:100',
            'descricao'=> 'max:255',
            'ativo'=> 'required' 
      
           
       ]);  
        // obtém todos os campos vindos do form
        $dados = $request->all();
        // se o usuário informou a foto e a imagem foi corretamente enviada
        
        $inc = Funcionario::create($dados);
        if ($inc) {
            return redirect()->route('funcionarios.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresas = Empresa::orderBy('nome')->get();
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Funcionario::find($id);
       
        return view('admin.funcionarios_form', ['reg' => $reg,'empresas' => $empresas,
                                           'acao' => 2]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'min:4|max:100',
            'empresas_id'=> 'required',
            'matricula'=> 'max:6',
            'profissao'=> 'required',
            'cpf'=> 'max:11',
            'endereco'=> 'max:100',
            'telefone'=> 'max:11',
            'email'=> 'max:100',
            'descricao'=> 'max:255',
            'ativo'=> 'required' 
      
           
       ]);
       
        // obtém os dados do form
        $dados = $request->all();
        // posiciona no registo a ser alterado
        $reg = Funcionario::find($id);
        // se o usuário informou a foto e a imagem foi corretamente enviada
      
        // realiza a alteração
        $alt = $reg->update($dados);
        if ($alt) {
            return redirect()->route('funcionarios.index')
                            ->with('status', $request->nome . ' Alterado!');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $funcionarios = Funcionario::find($id);
        if ($funcionarios->delete()) {
            return redirect()->route('funcionarios.index')
                            ->with('status', $funcionarios->nome . ' Excluído!');
        }
    }
    public function funcionariosFiltro()
    {
        $funcionarios = Funcionario::orderBy('nome')->get();
        return view('admin.funcionarios_filtro', ['funcionarios' => $funcionarios, 'acao' => 1,
           ]);

    }

    public function funcionariosRelatorio(Request $request)
    {
        $inicio = $request->inicio;
        $fim = $request->fim;
        $funcionarios = $request->funcionarios;
        $this->validate($request, [
            'inicio' => 'required',
            'fim' => 'required'

       ]); 
$SQL="SELECT DISTINCT f.nome, v.data, IF(f.profissao = 1,'MOTORISTA','COBRADOR') 
AS profissao from funcionarios f, viagens v WHERE f.id = $funcionarios and v.data >= '$inicio' 
and v.data <= '$fim' and (f.matricula = v.motorista or f.matricula = v.cobrador)";


$distfuncionarios= DB::select($SQL);
$numdias= "SELECT COUNT(DISTINCT v.data) as dias from funcionarios f, viagens v 
WHERE f.id = $funcionarios and v.data >= '$inicio' 
and v.data <= '$fim' and (f.matricula = v.motorista or f.matricula = v.cobrador)";    
 $dias= DB::select($numdias);
 return \PDF::loadView('admin.funcionarios_rel', ['distfuncionarios' => $distfuncionarios, 'dias'=>$dias, 'inicio'=>$inicio, 'fim'=>$fim])->stream();

    }


}
<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Chassi;
use App\User;
use Illuminate\Http\Request;
class ChassiController extends Controller
{
//
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dados = Chassi::paginate(30);
        $numChassis = Chassi::count('id');
        return view('admin.chassis_list', ['chassis' => $dados, 'numChassis'=>$numChassis]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        $users = User::orderBy('name')->get();
        $chassis = Chassi::orderBy('nome')->get();

        return view('admin.chassis_form', ['users'=> $users,'acao' => 1]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'min:4|max:100|unique:chassis',
            'descricao'=> 'max:250',
            'ativo'=> 'required' 
      
           
       ]);  
        // obtém todos os campos vindos do form
       
        $dados = $request->all();
        $inc = Chassi::create($dados);
        if ($inc) {
            return redirect()->route('chassis.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Chassi::find($id);
        $linhas = Chassi::orderBy('nome')->get();
        return view('admin.chassis_form', ['reg' => $reg, 'acao' => 2]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nome' => 'min:4|max:100|',
            'descricao'=> 'max:250',
            'ativo'=> 'required' 
           
       ]);     
           
        // obtém os dados do form
        $dados = $request->all();
        // posiciona no registo a ser alterado
        $reg = Chassi::find($id);
        // se o usuário informou a foto e a imagem foi corretamente enviada
        // realiza a alteração
        $alt = $reg->update($dados);
        if ($alt) {
            return redirect()->route('chassis.index')
                ->with('status', $request->nome . ' Alterado!');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chassis = Chassi::find($id);
        if ($chassis->delete()) {
            return redirect()->route('chassis.index')
                ->with('status', $chassis->nome . ' Excluído!');
        }
    }
}

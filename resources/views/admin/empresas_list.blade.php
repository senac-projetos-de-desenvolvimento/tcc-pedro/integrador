@extends('adminlte::page')

@section('title', 'Cadastro de Empresas')

@section('content_header')
<h1>Cadastro de Empresas
    <a href="{{ route('empresas.create') }}" class="btn btn-primary pull-right" role="button">Novo</a>
</h1>
@stop

@section('content')

@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif

<table class="table table-striped">
    <thead>
        <tr>


            <th>Empresa</th>
            <th>Endereço</th>
            <th>CEP</th>
            <th>CNPJ</th>
            <th>Telefone</th>
            <th>E-mail</th>
            <th>Obs.</th>
            <th>Alteração</th>
            <th>Status</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($empresas as $l)
        <tr>

            <td> {{$l->nome}} </td>
            <td> {{$l->endereco }} </td>
            <td> {{$l->cep }} </td>
            <td> {{$l->cnpj}} </td>
            <td> {{$l->telefone}} </td>
            <td> {{$l->email}} </td>
            <td> {{$l->descricao}} </td>
            <td> {{$l->users->name}} </td>
            <td> {{$l->ativo}} </td>
            <td>
                <a href="{{route('empresas.edit', $l->id)}}" class="btn btn-warning btn-sm" title="Alterar"
                    role="button"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;
              <!--  <form style="display: inline-block" method="post" action="{{route('empresas.destroy', $l->id)}}"
                    onsubmit="return confirm('Confirma Exclusão?')">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <button type="submit" title="Excluir" class="btn btn-danger btn-sm"><i
                            class="far fa-trash-alt"></i></button>
                </form> !-->
            </td>
            @if ($loop->iteration == $loop->count)
        <tr>
            <td colspan=8>Total de empresas cadastradas: {{$numEmpresas}}
            </td>
        </tr>
        @endif
        @empty
        <tr>
            <td colspan=8> Não há empresas cadastradas ou
                para o filtro informado </td>
        </tr>
        @endforelse

    </tbody>
</table>

{{ $empresas->links() }}
@stop

@section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection
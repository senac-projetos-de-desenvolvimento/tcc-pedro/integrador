@extends('adminlte::page')
@section('title', 'Filtro de Horários')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <h2>Filtro de Horários
       
        <a href="{{ route('horarios.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <form method="POST" action="{{ route('horariosImport') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
    <div class="container-fluid">
       
    <label for="util">Data dia Útil:</label>
                    <select id="uteis" name="uteis" class="form-control">
                            @foreach($viagens as $v)
                            <option value="{{$v->data}}" {{ ((isset($reg) and 
                            $reg->data == $v->data) or 
                         old('data') == $v->data) ? "selected" : "" }}>
                                {{$v->data}}</option>
                            @endforeach
                        </select>
                        <label for="sabado">Data Sábado:</label>
                        <select id="sabados" name="sabados" class="form-control">
                            @foreach($viagens as $v)
                            <option value="{{$v->data}}" {{ ((isset($reg) and 
                            $reg->data == $v->data) or 
                         old('data') == $v->data) ? "selected" : "" }}>
                                {{$v->data}}</option>
                            @endforeach
                        </select>
                        <label for="domingo">Data Domingo:</label>
                        <select id="domingos" name="domingos" class="form-control">
                            @foreach($viagens as $v)
                            <option value="{{$v->data}}" {{ ((isset($reg) and 
                            $reg->data == $v->data ) or 
                         old('data') == $v->data) ? "selected" : "" }}>
                                {{$v->data}}</option>
                            @endforeach
                        </select>
                
                <input type="submit" value="Importar" class="btn btn-success">        
               
                </div>

    
        
  
   
    </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
   
    });
    </script>
    @endsection
   
@extends('adminlte::page')
@extends('formulario')
@section('title', 'Cadastro de Veículos')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ($acao==1)
<h2>Inclusão de Veículos
    @elseif ($acao ==2)
    <h2>Alteração de Veículos
        @endif
        <a href="{{ route('veiculos.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <div class="container-fluid">
        @if ($acao==1)
        <form method="POST" action="{{ route('veiculos.store') }}" enctype="multipart/form-data">
        <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="prefixo">Prefixo:</label>
                            <input type="number" id="prefixo" name="prefixo" required
                                 class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="empresas_id">Empresa:</label>
                            <select id="empresas_id" name="empresas_id" class="form-control">
                                @foreach($empresas as $e)
                                <option value="{{$e->id}}">
                                    {{$e->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="banco">Assentos:</label>
                            <input type="number" id="banco" name="banco" 
                                class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="lotacao">Lotação:</label>
                            <input type="number" id="lotacao" name="lotacao" 
                                 class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="placa">Placa:</label>
                            <input type="text" id="placa" name="placa"onkeyup="maiuscula(this)
                                class="form-control">
                        </div>
                    </div>


                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="licenca_data">Data Venc. Licença:</label>
                            <input type="date" id="licenca_data" name="licenca_data" 
                                class="form-control">
                        </div>
                    </div>


                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="vistoria_data">Data Vistoria:</label>
                            <input type="date" id="vistoria_data" name="vistoria_data" 
                               class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="ano_modelo">Ano Modelo:</label>
                            <input type="number" id="ano_modelo" name="ano_modelo" 
                                 class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="ano_fabricacao">Ano Fabricação:</label>
                            <input type="number" id="ano_fabricacao" name="ano_fabricacao" 
                                 class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="chassis_id">Chassi:</label>
                            <select id="chassis_id" name="chassis_id" class="form-control">
                                @foreach($chassis as $c)
                                <option value="{{$c->id}}" >
                                    {{$c->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="carrocerias_id">Carroceria:</label>
                            <select id="carrocerias_id" name="carrocerias_id" class="form-control">
                                @foreach($carrocerias as $c)
                                <option value="{{$c->id}}" >
                                    {{$c->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="descricao">Descrição:</label>
                            <textarea input type="text" id="descricao" name="descricao" 
                              class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-2">
                    <div class="form-group">
                        <label for="acessibilidade">Acessibilidade:</label><br>
                        <select name="acessibilidade" class="form-control">
                            <option selected="">
                            </option>
                            <option value="Sim">Sim</option>
                            <option value="Não">Não</option>
                        </select>
                    </div>
                    </div>

                    <div class="col-sm-2">
                        <label for="ativo">Status:</label><br>
                        <select name="ativo" class="form-control">
                            <option selected="">
                            </option>
                            <option value="Ativo">Ativo</option>
                            <option value="Inativo">Inativo</option>
                        </select>
                    </div>
                </div>
            @elseif ($acao==2)
            <form method="POST" action="{{route('veiculos.update', $reg->id)}}" enctype="multipart/form-data">
                {!! method_field('put') !!}
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="prefixo">Prefixo:</label>
                            <input type="number" id="prefixo" name="prefixo" required
                                value="{{$reg->prefixo}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="empresas_id">Empresa:</label>
                            <select id="empresas_id" name="empresas_id" class="form-control">
                                @foreach($empresas as $e)
                                <option value="{{$e->id}}" {{ ((isset($reg) and
                                 $reg->empresas_id == $e->id) == $e->id) ? "selected" : "" }}>
                                    {{$e->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="banco">Assentos:</label>
                            <input type="number" id="banco" name="banco" 
                                value="{{$reg->banco}}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="lotacao">Lotação:</label>
                            <input type="number" id="lotacao" name="lotacao" 
                                value="{{$reg->lotacao}}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="placa">Placa:</label>
                            <input type="text" id="placa" name="placa" onkeyup="maiuscula(this)" value="{{$reg->placa}}"
                                class="form-control">
                        </div>
                    </div>


                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="licenca_data">Data Venc. Licença:</label>
                            <input type="date" id="licenca_data" name="licenca_data" 
                                value="{{$reg->licenca_data}}" class="form-control">
                        </div>
                    </div>


                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="vistoria_data">Data Vistoria:</label>
                            <input type="date" id="vistoria_data" name="vistoria_data" 
                                value="{{$reg->vistoria_data}}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="ano_modelo">Ano Modelo:</label>
                            <input type="number" id="ano_modelo" name="ano_modelo" 
                                value="{{$reg->ano_modelo}}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="ano_fabricacao">Ano Fabricação:</label>
                            <input type="number" id="ano_fabricacao" name="ano_fabricacao" 
                                value="{{$reg->ano_fabricacao}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="chassis_id">Chassi:</label>
                            <select id="chassis_id" name="chassis_id" class="form-control">
                                @foreach($chassis as $c)
                                <option value="{{$c->id}}" {{ ((isset($reg) and
                                 $reg->chassis_id == $c->id) == $c->id) ? "selected" : "" }}>
                                    {{$c->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="carrocerias_id">Carroceria:</label>
                            <select id="carrocerias_id" name="carrocerias_id" class="form-control">
                                @foreach($carrocerias as $c)
                                <option value="{{$c->id}}" {{ ((isset($reg) and
                                 $reg->carrocerias_id == $c->id) == $c->id) ? "selected" : "" }}>
                                    {{$c->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="descricao">Descrição:</label>
                            <textarea input type="text" id="descricao" name="descricao" 
                                value="{{$reg->descricao}}" class="form-control">{{$reg->descricao }}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-2">
                    <div class="form-group">
                        <label for="acessibilidade">Acessibilidade:</label><br>
                        <select name="acessibilidade" class="form-control">
                            <option selected="{{$reg->acessibilidade}}">{{$reg->acessibilidade}}
                            </option>
                            <option value="Sim">Sim</option>
                            <option value="Não">Não</option>
                        </select>
                    </div>
                    </div>

                    <div class="col-sm-2">
                        <label for="ativo">Status:</label><br>
                        <select name="ativo" class="form-control">
                            <option selected="{{$reg->ativo}}">{{$reg->ativo}}
                            </option>
                            <option value="Ativo">Ativo</option>
                            <option value="Inativo">Inativo</option>
                        </select>
                    </div>
                </div>
                @endif
                {{ csrf_field() }}
               
                <div class="form-group">
                        <input type="hidden" id="users_id" name="users_id" required value="{{ Auth::user()->id }}">
                        <input type="submit" value="Enviar" class="btn btn-success">
                        <input type="reset" value="Limpar" class="btn btn-warning">
            </form>
    
    </div>
    @endsection

    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>


    @endsection
   
@extends('formulario')



        <style type="text/css">
       @page {
            margin: 20px 50px 80px 50px;
        }
        #head{
            background-repeat: no-repeat;
            position: relative;
            text-align: center;
            border-top: 1px solid gray;
            height: 110px;
            width: 100%;
            top: 10px;
            left: 0;
            right: 0;
            margin: auto;
        }
        #head .page:after{

            content: counter(page);
        }
        #corpo{
          top: 80px;
            
            position: relative;
            margin: auto;
        }
        table{
            border-collapse: collapse;
            width: 100%;
            position: relative;
        }
        td{
          
        }
        #footer {
          background-repeat: no-repeat;
            position: relative;
            bottom: 0;
            width: 100%;
            text-align: right;
            margin: 0px 0px 0px -40px;
        }
        #footer .page:after{
            content: counter(page);
        }
        p {
          text-align: left;
          border-bottom: 1px solid gray;
  }
        </style></head><body>


<div id="head">
    <img src="{{url('storage/fotos/cabecalho.png')}}"/>
    </div>
    <div id="corpo">
    <h1>Relatório de Atendimentos</h1>
    <table   >
    @foreach ($atendimentos as $a)
        <tr>
        
        <th colspan="30"  >Atendimento: {{$a->codatendimento}} / {{$a->nome}}
        <table align="center" >
      
          <tr>
          <td> 
                <img src="{{url('storage/'.$a->foto)}}" style="width: 800px; height: 480px" alt="Foto" >
                </td>
          
          @endforeach
          </tr>
          <tr>

          </th>
          <th>
          <p>Linha: {{$a->linha}}</p>
        <p>Km de Ida: {{$a->km_ida}} | Km de Volta: {{$a->km_volta}} </p>
          <p>Descrição: {{$a->descricao}} </p>
          @foreach ($kmui as $kui)
          <p>Quantidade de voltas nos dias úteis, sentido ida: {{$kui->km}} | Totalizando {{$kui->km * $a->km_ida}}Km </p>
          @endforeach
          @foreach ($kmuv as $kuv)
          <p>Quantidade de voltas nos dias úteis, sentido volta: {{$kuv->km}} | Totalizando {{$kuv->km * $a->km_volta}}Km </p>
          @endforeach 
           @foreach ($kmsi as $ksi)
          <p>Quantidade de voltas nos sábados, sentido ida: {{$ksi->km}} | Totalizando {{$ksi->km * $a->km_ida}}Km</p>
          @endforeach
           @foreach ($kmsv as $ksv)
          <p>Quantidade de voltas nos sábados, sentido volta: {{$ksv->km}} | Totalizando {{$ksv->km * $a->km_volta}}Km</p>
          @endforeach
           @foreach ($kmdi as $kdi)
          <p>Quantidade de voltas nos domingos, sentido ida: {{$kdi->km}} | Totalizando {{$kdi->km * $a->km_ida}}Km </p>
          @endforeach
          @foreach ($kmdv as $kdv)
          <p>Quantidade de voltas nos domingos, sentido volta: {{$kdv->km}} | Totalizando {{$kdv->km * $a->km_volta}}Km</p>
          @endforeach
        
          <p>Total de Km nos dias úteis: {{($kui->km * $a->km_ida)+($kuv->km * $a->km_volta)}} |
          Total de Km nos sábados: {{($ksi->km * $a->km_ida)+($ksv->km * $a->km_volta)}} |
          Total de Km nos domingos: {{($kdi->km * $a->km_ida)+($kdv->km * $a->km_volta)}} </p>
        
          </th>
          </table>
               
        </div>
        
        
        
    
    </body>

    <div id="footer"><?php
date_default_timezone_set('America/Bahia');
echo date('d/m/Y - H:i:s');
?></div>









    @section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection




</html>
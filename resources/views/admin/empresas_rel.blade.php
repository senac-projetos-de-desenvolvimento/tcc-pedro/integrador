@extends('formulario')



        <style type="text/css">
       @page {
            margin: 120px 50px 80px 50px;
        }
        #head{
            background-repeat: no-repeat;
            position: fixed;
            text-align: right;
            border-top: 1px solid gray;
            height: 110px;
            width: 100%;
            top: -100px;
            left: 0;
            right: 0;
            margin: auto;
        }
        #head .page:after{

            content: counter(page);
        }
        #corpo{
          top: 60px;
            
            position: relative;
            margin: auto;
        }
        table{
            border-collapse: collapse;
            width: 100%;
            position: relative;
        }
        td{

        }
        #footer {
          background-repeat: no-repeat;
            position: fixed;
            bottom: 0;
            width: 100%;
            text-align: right;
            border-top: 1px solid gray;
        }
        #footer .page:after{
            content: counter(page);
        }
        </style></head><body>


    <div id="head"><p class="page">Página </p>
    <img src="storage/fotos/cabecalho.png"/>

    </div>
    <div id="corpo">
    <h4>Filtro:</h4p>
    <h4>Relatório da Quilometragem Percorrida pelas Empresas</h4>
    <p>Data Início: {{$inicio}} / Data Final: {{$fim}}</p>
    <h4> Total de km rodado de cada empresa por dia:</h4>
    <table align="center" >
    @foreach ($kmdias as $d)
        <tr>
          <td colspan="20" >{{$d->nome}}</td>
          <td colspan="20" >{{$d->data}}</td>
          <td colspan="20" >{{number_format($d->KM, 1, ',', '.')}}</td>
         
          </tr>
          
          @endforeach
          </table>
         <h4"> Total por Empresa:</h4>
        <table align="center" >
            
        <tr>
          <th colspan="20">Empresa:</th>
        <th colspan="20">Km:</th>
        
        </tr>
        @foreach ($kmempresas as $k)
        <tr>
          <td colspan="20" >{{$k->nome}}</td>
          <td colspan="20" >{{number_format($k->KM, 1, ',', '.')}}</td>
         
          </tr>
      
          @endforeach
          </table>
          @foreach ($kmsoma as $s)
         <h4> Total de KM rodado: {{number_format($s->KM, 1, ',', '.')}}
         @endforeach
        </div>
        
        
        <div id="footer"><?php
date_default_timezone_set('America/Bahia');
echo date('d/m/Y - H:i:s');
?></div>
    
    </body>
















</html>